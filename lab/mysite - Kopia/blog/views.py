from django.contrib.auth.models import User
from models import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404,redirect, render, render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from forms import *
import forms


def entires_view(request, username):
    try:
        usr = User.objects.get(username=username)
        entries = Entry.objects.filter(author = usr.pk).order_by('-date')
        return render(request, 'blog/userentries.html', {
            'entries': entries,
            'author': username,
            })
    except Exception as ex:
        messages.add_message(request,messages.INFO, ex)
        return redirect('home')


def blog_add(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = EntryForm(data=request.POST)
            if form.is_valid():
                data=form.cleaned_data
                entry = Entry(author=User.objects.get(username=request.user), title=data['title'], text=data['text'])
                entry.full_clean()
                entry.save()
                url = reverse('home')
                messages.add_message(request, messages.INFO, 'The entry has been added successfully')
                return HttpResponseRedirect(redirect_to=url)
            else:
                messages.add_message(request, messages.INFO, 'message too long or too short')
                return redirect('blog/add.html')

        else:
            return render(request, 'blog/add.html')
    except PermissionDenied:
        messages.add_message(request, messages.INFO, 'You have no permission to add entry')
        return redirect('home')

def blog_add_(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            title = request.POST.get('title', 0)
            text = request.POST.get('text', 0)
            author = User.objects.get(username=request.user)
            entry = Entry(title=title,text=text,author=author)
            entry.full_clean()
            entry.save()
            url = reverse('home')
            messages.add_message(request, messages.INFO, 'The entry has been added successfully')
            return HttpResponseRedirect(redirect_to=url)
        else:
            return render(request, 'blog/add.html')
    except PermissionDenied:
        messages.add_message(request, messages.INFO, 'You have no permission to add entry')
        return redirect('home')
def blog_signup(request):
    if(request.method == 'POST'):
        username = request.POST.get('username',0)
        password = request.POST.get('password',0)
        try:
            user = User.objects.create_user(username=username, password=password)
            user.full_clean()
            user.save()
            message = 'Congratulations! You have successfully registered'
            messages.add_message(request, messages.INFO, message)
            return redirect('home')
        except Exception as ex :
            messages.add_message(request, messages.INFO, 'User with username "%s" already exist'%username)
            return redirect('blog_signup')
    else:
        return render(request, 'blog/signup.html')

def blog_login(request):
    if(request.method == 'POST'):
        username = request.POST.get('username',0)
        password = request.POST.get('password',0)
        user = authenticate(username=username, password=password)
        if user is not None:
            messages.add_message(request, messages.INFO, 'You have been successfully logged in' )
            login(request, user)
            return redirect('home')
        else:
            messages.add_message(request, messages.INFO, 'Invalid username or password' )
            return redirect('blog_login')
    else:
        return render(request, 'blog/login.html')

def blog_logout(request):
    if request.user.is_authenticated():
        logout(request)
        messages.add_message(request, messages.INFO, 'You have been successfully logged out' )
    else:
        pass
    return redirect('home')