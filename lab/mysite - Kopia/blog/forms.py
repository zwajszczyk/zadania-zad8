__author__ = 'Zbyszek'
from django.forms import ModelForm, Form
from models import Entry
from django.contrib.auth.models import User
from django import forms

class EntryForm(Form):
    title = forms.CharField(label='title')
    text = forms.CharField(label='text', min_length=10, max_length=200, widget=forms.Textarea)