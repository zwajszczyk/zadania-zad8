__author__ = 'Zbyszek'

from django.contrib import admin
from models import Entry, Tag

class EntryAdmin(admin.ModelAdmin):
    list_display = ('title','author', 'date', 'tagnames',)
    list_filter = ('date',)
    ordering = ('date',)
    search_fields = ('tags__text',)

admin.site.register(Tag)
admin.site.register(Entry, EntryAdmin)
