"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from models import Entry
from django.test.client import Client
from models import User
from django.contrib.auth.models import User, UserManager


class SimpleTest(TestCase):

    def setUp(self):
        self.client = Client()
        #self.userManager = UserManager()
        self.testUser1 = User.objects.get_or_create(username="testowy", password="admin123")
        self.testUser2 = User.objects.get_or_create(username="testowy2", password="qwerty")
        #print User.objects.all()
        self.testTitle1 = "TTT"
        self.testText1 = "XXX"
        self.testAuthorName1 = "testowy"
        self.testPassword1 = "admin123"
        self.testTitle2 = "WWW2"
        self.testText2 = "YYY2"
        self.testAuthorName2 = "testowy2"
        self.testPassword2 = "qwerty"
        self.testEntry1 = Entry.objects.get_or_create(title = "TTT", text="XXX", author = User.objects.get(username="testowy"))
        self.testEntry2 = Entry.objects.get_or_create(title = "WWW2", text="YYY2", author = User.objects.get(username="testowy2"))

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)



    def test_show(self):
        resp = self.client.get('')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(self.testTitle1 and self.testText1 and self.testAuthorName1 in unicode(resp))

    def test_login_good(self):
        #resp = self.client.post('/blog/login', {'username': "testowy", 'password': "admin123" },follow=True)
        resp = self.client.get('/blog/login',follow=True)
        print unicode(resp)
        #self.client.login(username='testowy', password='admin123')
        session = self.client.session
        #self.assertIn('_auth_user_id', self.client.session)
        print session


