from django.contrib.auth.models import User
from models import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404,redirect, render, render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from forms import *
import forms
from django.template import RequestContext
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone


def entires_view(request, username):
    try:
        usr = User.objects.get(username=username)
        entries = Entry.objects.filter(author = usr.pk).order_by('-date')
        return render(request, 'blog/userentries.html', {
            'entries': entries,
            'author': username,
            })
    except Exception as ex:
        messages.add_message(request,messages.INFO, ex)
        return redirect('home')

def tag_entires_view(request, tagtext):
    try:
        tag = Tag.objects.get(text=tagtext)
        entries = tag.entry_set.all().order_by('-date')
        return render(request, 'blog/tagentries.html', {
            'entries': entries,
            'tag': tagtext,
            })
    except Exception as ex:
        messages.add_message(request,messages.INFO, ex)
        return redirect('home')

def entry_edit_view(request, entry_id):
    try:
        #entry_id=1
        if not request.user.is_authenticated(): raise PermissionDenied
        entry = Entry.objects.get(pk=entry_id)
        if (request.method=='POST'):
            form = EntryEditForm(data=request.POST)
            if form.is_valid():
                current_user = request.user
                user_is_moderator = current_user.groups.filter(name='moderatorzy').exists()
                if user_is_moderator or (current_user==entry.author and entry.is_new()):
                    entry.text = form.cleaned_data['text']
                    entry.last_edit_date = timezone.now()
                    entry.last_editor_name = current_user.username
                    entry.full_clean()
                    entry.save()
                    url = reverse('home')
                    messages.add_message(request, messages.INFO, 'edit successful')
                    return HttpResponseRedirect(redirect_to=url)
                else:
                    messages.add_message(request, messages.INFO, 'you cannot edit now')
                    #return render_to_response('blog/add2.html', locals())
                    return redirect('home')
            else:
                messages.add_message(request, messages.INFO, 'form is invalid')
                return redirect('home')
        else:
        #entry = Entry.objects.get(pk=entry_id)
            current_user = request.user
            user_is_moderator = current_user.groups.filter(name='moderatorzy').exists()
            if user_is_moderator or (current_user==entry.author and entry.is_new()):
                form = EntryEditForm(initial={'text': entry.text})
                return render_to_response('blog/edit.html', {'form': form, 'eid': entry_id}, context_instance=RequestContext(request))
                #return redirect('blog/list.html')
                #return HttpResponseRedirect(reverse('blog.views.entry_edit_view', args=[form]))
            else:
                messages.add_message(request, messages.INFO, 'you cannot edit now')
                return redirect('home')
    except Exception as ex:
        messages.add_message(request,messages.INFO, ex)
        return redirect('home')



def blog_add(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = EntryForm(data=request.POST)
            if form.is_valid():
                data=form.cleaned_data
                tagtexts = str(data['tags']).split()
                entry = Entry(author=User.objects.get(username=request.user), title=data['title'], text=data['text'])
                entry.save()
                for tagtext in tagtexts:
                    temp_taglist=list(Tag.objects.filter(text=tagtext)[:1])
                    if temp_taglist:
                        temp_tag=temp_taglist[0]
                    else:
                    #print tag
                        temp_tag=Tag.objects.create(text=tagtext)
                        temp_tag.save()
                    entry.tags.add(temp_tag)
                entry.full_clean()
                entry.save()
                url = reverse('home')
                messages.add_message(request, messages.INFO, 'The entry has been added successfully')
                return HttpResponseRedirect(redirect_to=url)
                #return render_to_response(url, locals())# context_instance=RequestContext(request))
            else:
                messages.add_message(request, messages.INFO, 'message too long or too short')
                #return render_to_response('blog/add2.html', locals())
                return redirect('blog/add2.html')

        else:
            form = EntryForm()
            #return render(request, 'blog/add2.html')
            return render_to_response('blog/add2.html', locals(),context_instance=RequestContext(request))
    except PermissionDenied:
        messages.add_message(request, messages.INFO, 'You have no permission to add entry')
        return redirect('home')

def blog_add_(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            title = request.POST.get('title', 0)
            text = request.POST.get('text', 0)
            author = User.objects.get(username=request.user)
            entry = Entry(title=title,text=text,author=author)
            entry.full_clean()
            entry.save()
            url = reverse('home')
            messages.add_message(request, messages.INFO, 'The entry has been added successfully')
            return HttpResponseRedirect(redirect_to=url)
        else:
            return render(request, 'blog/add.html')
    except PermissionDenied:
        messages.add_message(request, messages.INFO, 'You have no permission to add entry')
        return redirect('home')
def blog_signup(request):
    if(request.method == 'POST'):
        form = UserCreationForm(request.POST)
        #username = request.POST.get('username',0)
        #password = request.POST.get('password',0)
        if form.is_valid():
            new_user = form.save()
            messages.add_message(request, messages.INFO, 'thanx for registration')
            return HttpResponseRedirect('home')
        else:
            messages.add_message(request, messages.INFO, 'form is invalid')
            return HttpResponseRedirect('home')

    else:
        form = UserCreationForm()
        #return render(request, 'blog/add2.html')
        return render_to_response('blog/signup.html', locals(),context_instance=RequestContext(request))

def blog_signup_old(request):
    if(request.method == 'POST'):
        form = UserCreationForm(request.POST)
        username = request.POST.get('username',0)
        password = request.POST.get('password',0)
        try:
            user = User.objects.create_user(username=username, password=password)
            user.full_clean()
            user.save()
            message = 'Congratulations! You have successfully registered'
            messages.add_message(request, messages.INFO, message)
            return redirect('home')
        except Exception as ex :
            messages.add_message(request, messages.INFO, 'User with username "%s" already exist'%username)
            return redirect('blog_signup')
    else:
        form = UserCreationForm()
        #return render(request, 'blog/add2.html')
        return render_to_response('blog_signup', locals(),context_instance=RequestContext(request))

def blog_login(request):
    if(request.method == 'POST'):
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = request.POST.get('username',0)
            password = request.POST.get('password',0)
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                messages.add_message(request, messages.INFO, 'You have been successfully logged in' )
                login(request, user)
                return redirect('home')
            else:
                messages.add_message(request, messages.INFO, 'Invalid username or password' )
                return redirect('blog_login')
        else:
            messages.add_message(request, messages.INFO, 'Form is invalid' )
            return redirect('blog_login')
    else:
        form=LoginForm()
        return render_to_response('blog/login.html', locals(),context_instance=RequestContext(request))

def blog_login_old(request):
    if(request.method == 'POST'):
        username = request.POST.get('username',0)
        password = request.POST.get('password',0)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                messages.add_message(request, messages.INFO, 'You have been successfully logged in' )
                login(request, user)
                return redirect('home')
            else:
                messages.add_message(request, messages.INFO, 'User is inactive. Finish your registration.' )
                return redirect('blog_login')
        else:
            messages.add_message(request, messages.INFO, 'Invalid username or password' )
            return redirect('blog_login')
    else:
        return render(request, 'blog/login.html')

def blog_logout(request):
    if request.user.is_authenticated():
        logout(request)
        messages.add_message(request, messages.INFO, 'You have been successfully logged out' )
    else:
        pass
    return redirect('home')