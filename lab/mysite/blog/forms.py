__author__ = 'Zbyszek'
from django.forms import ModelForm, Form
from models import Entry
from django.contrib.auth.models import User
from django import forms

class EntryForm(forms.Form):
    title = forms.CharField(label='title')
    text = forms.CharField(label='text', min_length=10, max_length=200, widget=forms.Textarea)
    tags = forms.CharField(label='tags')

class EntryEditForm(forms.Form):
    text = forms.CharField(label='text', min_length=10, max_length=200, widget=forms.Textarea)

class LoginForm(forms.Form):
    username = forms.CharField(label="username")
    password = forms.CharField(widget=forms.PasswordInput, label = 'password')
