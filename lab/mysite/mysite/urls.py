from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
     url(r'^blog/', include('blog.urls')),
     url(r'^$', include('blog.urls')),
)
urlpatterns += staticfiles_urlpatterns()
