from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
from models import *
from views import *
from django.contrib import admin
admin.autodiscover()
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', ListView.as_view(
                           queryset=Entry.objects.order_by('-date'),
                           context_object_name='entries',
                           template_name="blog/list.html"
                       ), name='home'),
                       url(r'^add/',blog_add, name='blog_add'),
                       url(r'^login/',blog_login, name='blog_login'),
                       url(r'^logout/',blog_logout, name='blog_logout'),
                       url(r'^signup/',blog_signup, name='blog_signup'),
                       url(r'^entries/(?P<username>\w+)/$', entires_view, name='entries_view' ),
                       url(r'^tags/(?P<tagtext>\w+)/$', tag_entires_view, name='tag_entries_view' ),
                       url(r'^edit/(?P<entry_id>\w+)/$', entry_edit_view, name='entry_edit' ),
)


