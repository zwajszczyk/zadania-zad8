from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Tag(models.Model):
    text = models.CharField(max_length=20)

    def __unicode__(self):
        return self.text

class Entry(models.Model):
    title = models.CharField(max_length=40)
    text = models.CharField(max_length=400)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User)
    tags = models.ManyToManyField(Tag)
    last_edit_date = models.DateTimeField(null=True, blank=True)
    last_editor_name = models.CharField(max_length=100, null=True, blank=True)

    def is_new(self):
        if self.last_edit_date is not None:
            modify_date = self.last_edit_date
        else:
            modify_date = self.date
        if (timezone.now()-modify_date).seconds < 600:
            return True
        else:
            return False



    def tagnames(self):
        return ', '.join([t.text for t in self.tags.all()])

    def __unicode__(self):
        return self.title


